<?php
App::uses('AppModel', 'Model');
/**
 * This source code is licensed under the EUPL, Version 1.1 only (the “Licence”).
 * You may not use, modify or distribute this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * <http://joinup.ec.europa.eu/software/page/eupl/licence-eupl>
 * A copy is also distributed with this source code.
 * Unless required by applicable law or agreed to in writing, software distributed under the
 * Licence is distributed on an “AS IS” basis, without warranties or conditions of any kind.
 */

/**
 * Question Model
 *
 * @property Answer $Answer
 */
class Question extends AppModel {
/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'maturity' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'Answer' => array(
			'className' => 'Answer',
			'foreignKey' => 'question_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

    public function pdfData($array = false) {
        /* Get data in questionid->value or
         * questionid->array(index->value)
         * Parse input, show questions and answers in their places.
         * If an answer had {input}, throw the input user gave into that place.
         */
        if(is_array($array)) {
            $tmp = array();
            $score = array();
            $score['value'] = -1;
            $score['id'] = -1;
            foreach($array as $key => $val) {
            if($key != "reportid" && $key != "category" && $key != "kypsyysaste"){
            	if(!preg_match("/score/i", $key)) {
            		$obj = $this->find('first', array('conditions' => array('Question.id' => $key), 'recursive' => '1'));
                	$tmp[$key]['question'] = $obj['Question']['question'];
            	}
				if(preg_match("/score/i", $key)) {
					$score['value'] = $val;
					$tempid = explode('_', $key);
					$score['id'] = $tempid[1];
            	} else if(!is_array($val)) {
                    $itmp = explode('|', $val);
                    $tmp[$key]['answer'] = $obj['Answer'][$itmp[0]]['value'];
                    if(isset($itmp[1])) {
                        $tmp[$key]['answer'] = str_replace($itmp[1], $itmp[2], $tmp[$key]['answer']);
                    }
                } else if(is_array($val)) {
                    /* If there is more than one {input} in one answer, storing added indexes
                     * prevents adding that answer again and creating confusion
                     */
                    $indexes = array();
                    foreach($val as $i => $valA) {
                        $itmp = explode('|', $valA);
                        if(!isset($indexes[$itmp[0]])) {
                            if(!isset($tmp[$key]['answer'])) {
                                $tmp[$key]['answer'] = $obj['Answer'][$itmp[0]]['value'];
                            } else {
                                $tmp[$key]['answer'] = $tmp[$key]['answer'].'<br/> '.$obj['Answer'][$itmp[0]]['value'];
                            }
                            $indexes[$itmp[0]] = '-';
                        }
                        if(isset($itmp[1])) {
                                $tmp[$key]['answer'] = str_replace($itmp[1], $itmp[2], $tmp[$key]['answer']);
                        }
                    }
                } else if(!empty($val)) {
                    $tmp[$key]['answer'] = $obj['Answer'][$val]['value'];
                }
                if($score['value'] > -1 && $score['id'] == $key) {
                	$tmp[$key]['score'] = $score['value'];
                	$score['value'] = -1;
                	$score['id'] = -1;
                }
            }
            }
            return $tmp;
        }
        return false;
    }

    function beforeSave($options) {
        /* questions -table has category and type -data in text format, but maturity in int
         * Since the listbox used in questions/add returns an index by default
         * we'll have to swap categories and types from integer indexes to their word form.
         */
        $category = array('toiminta', 'case', 'opetus');
        $type = array('radio', 'checkbox', 'inputbox');
        $this->data['Question']['category'] = $category[$this->data['Question']['category']];
        $this->data['Question']['type'] = $type[$this->data['Question']['type']];
        return true;
    }

}
