<?php

App::uses('AppModel', 'Model');

/**
 * This source code is licensed under the EUPL, Version 1.1 only (the “Licence”).
 * You may not use, modify or distribute this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * <http://joinup.ec.europa.eu/software/page/eupl/licence-eupl>
 * A copy is also distributed with this source code.
 * Unless required by applicable law or agreed to in writing, software distributed under the
 * Licence is distributed on an “AS IS” basis, without warranties or conditions of any kind.
 */

class User extends AppModel {
	
	public $hasMany = array(
			'Feedback' => array(
					'className' => 'Feedback',
					'foreignKey' => 'userid',
					'dependent' => false,
					'conditions' => '',
					'fields' => '',
					'order' => '',
					'limit' => '',
					'offset' => '',
					'exclusive' => '',
					'finderQuery' => '',
					'counterQuery' => ''
			)
	);

    var $validate = array(
                    'username' => array(
                                    'email' => array(
                                                    'rule'      => 'email',
                                                    'message'   => 'Oltava validi sähköpostiosoite',
                                    ),
                                    'unique' => array(
                                                    'rule'      => 'isUnique',
                                                    'message'   => 'Käyttäjätunnus jo käytössä',
                                    ),
                    ),
                    'password' => array(
                                    'empty' => array(
                                                    'rule'      => 'notEmpty',
                                                    'message'   => 'Ei saa olla tyhjä',
                                                    'required'  => true,
                                    ),
                    ),
                    'password_confirm' => array(
                                    'compare'    => array(
                                                    'rule'      => array('password_match', 'password'),
                                                    'message'   => 'Kirjoittamasi salasana ei täsmää',
                                                    'required'  => true,
                                    ),
                                    'length' => array(
                                                    'rule'      => array('between', 6, 20),
                                                    'message'   => 'Salasanan pituus välillä 6 ja 20 merkkiä',
                                    ),
                                    'empty' => array(
                                                    'rule'      => 'notEmpty',
                                                    'message'   => 'Ei saa olla tyhjä',
                                    ),
                    ),
    );

    public function beforeSave($options = array()) {
        if(isset($this->data['User']['password'])){
            $this->data['User']['password'] = AuthComponent::password($this->data['User']['password']);
        }
        return true;
    }

    function password_match($data, $password_field) {
        $password = $this->data[$this->alias][$password_field];
        $keys = array_keys($data);
        $password_confirm = $data[$keys[0]];
        return $password === $password_confirm;
    }

}
