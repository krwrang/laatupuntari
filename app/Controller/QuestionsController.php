<?php
/**
 * Created by Iiro Uusitalo <iiro.uusitalo@samk.fi>.
 * Date: 25.11.2011
 * Time: 14.55
 * 
 * This source code is licensed under the EUPL, Version 1.1 only (the “Licence”).
 * You may not use, modify or distribute this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * <http://joinup.ec.europa.eu/software/page/eupl/licence-eupl>
 * A copy is also distributed with this source code.
 * Unless required by applicable law or agreed to in writing, software distributed under the
 * Licence is distributed on an “AS IS” basis, without warranties or conditions of any kind.
 */

class QuestionsController extends AppController {

    function toiminta() {
        //This did nothing, since the data is fetched in getData()
        //$this->set('data', $this->Question->find('all', array('conditions' => array('Question.type' => '0'), 'recursion' => '2')));
    }

    function kase() {

    }

    function opetus() {

    }

    function start() {

    }

    function valmis() {

    }

    function add() {
        /* add is used both in showing the form for addition and saving the data.
         * check requesthandler, if there is data to save
         */
        if($this->RequestHandler->isPost() && !empty($this->data)) {
            $this->Question->saveAll($this->data);
        }
    }

    /*
     * Read question-data recursively from database, then send it with the help of JSON
     */    
    function getData() {
        $this->autoRender = false;
		//pr($this->request->query['maturity']);
        if($this->request->is('ajax')) {
            $questions = $this->Question->find('all', array(
                            'order' => array('Question.sort'),
                            'conditions' => array(
                                            'Question.maturity' => $this->request->query['maturity'],
                                            'Question.category' => $this->request->query['category']),
                            'recursive' => '1'
            ));
            $questions = $this->maxWeight($questions);
            echo json_encode($questions);
        } else {
            $this->redirect($this->referer());
        }
    }

    function createpdf() {
        if($this->RequestHandler->isPost() && !empty($this->data) && count($this->data) < 100) {
            $tmp = array();
            $tmp = $this->parseData($tmp, 1);
            $kypsyys['toiminta'] = array('Valmisteilla', 'Käynnistymässä', 'Käynnissä');
            $kypsyys['case'] = array('Tilausvaihe', 'Käynnistysvaihe', 'Toteutusvaihe', 'Arviointivaihe');
            $kypsyys['opetus'] = array('Opetuksen integroinnin suunnittelu', 'Toteutus', 'Arviointi ja palaute', 'Living Lab -toiminnan ja opetuksen kehittäminen');
            $kategoria = array('toiminta' => 'Living lab -toiminta',
            				   'case' => 'Living lab -case',
            				   'opetus' => 'Living Lab ja opetus');
			//pr($this->data);
            if(is_array($this->data['kypsyysaste'])) {
            	$kypsyysaste = '';
            	foreach($this->data['kypsyysaste'] as $index => $val){
            		$kypsyysaste = $kypsyysaste.' '.$kypsyys[$this->data['category']][$val].',';
            	}
            	$kypsyysaste = substr($kypsyysaste, 0, strlen($kypsyysaste) - 1);
            	$this->set('kypsyys', $kypsyysaste);
            } else {
            	$this->set('kypsyys', $kypsyys[$this->data['category']][$this->data['kypsyysaste']]);
            }
            $this->set('category', $kategoria[$this->data['category']]);
            $this->set('data', $this->Question->pdfData($tmp));
            $this->layout = 'pdf';
        } else {
        	$this->Session->setFlash('PDF-raportin luominen epäonnistui!', 'default', array(), 'auth');
        	$this->redirect($this->referer());
        }
    }

    function debugMaxScore() {
        $questions = $this->Question->find('all', array(
           'order' => array('Question.sort'),
           'conditions' => array(
                           'Question.maturity' => '0',
                           'Question.category' => 'toiminta'),
           'recursive' => '1'
        ));

        $questions = $this->maxWeight($questions);

        pr($questions);
    }
    
    function debugAll(){
    	$questions['toiminta'] = $this->Question->find('all', array(
    			'order' => array('Question.sort'),
    			'conditions' => array(
    					'Question.category' => 'toiminta'),
    			'recursive' => '1'
    	));
    	$questions['case'] = $this->Question->find('all', array(
    			'order' => array('Question.sort'),
    			'conditions' => array(
    					'Question.category' => 'case'),
    			'recursive' => '1'
    	));
    	$questions['opetus'] = $this->Question->find('all', array(
    			'order' => array('Question.sort'),
    			'conditions' => array(
    					'Question.category' => 'opetus'),
    			'recursive' => '1'
    	));
    	$this->set('questions', $questions);
    }
}
