<?php
/**
 * Created by Kristian Wrang <kristian.2.wrang@samk.fi>.
 * Date: 13.3.2012
 * Time: 13:01
 * 
 * This source code is licensed under the EUPL, Version 1.1 only (the “Licence”).
 * You may not use, modify or distribute this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * <http://joinup.ec.europa.eu/software/page/eupl/licence-eupl>
 * A copy is also distributed with this source code.
 * Unless required by applicable law or agreed to in writing, software distributed under the
 * Licence is distributed on an “AS IS” basis, without warranties or conditions of any kind.
 */

class UsersController extends AppController {
    var $name = 'Users';

    public function login() {
        if(!empty($this->data)) {
        	if($this->Auth->login()) {
	            return $this->redirect($this->Auth->redirect());
	        } else {
	            $this->Session->setFlash('Tarkista sähköpostiosoite ja salasana', 'default', array(), 'auth');
	        }
        }
    }

    function logout() {
        $this->redirect($this->Auth->logout());
    }
    
    function info() {
    	
    }

    function register() {
        if(!empty($this->data)) {
            if ($this->User->save($this->data)) {
                $this->Session->setFlash('Tilisi on luotu', 'default', array(), 'auth');
                $login = $this->Auth->login();
                $this->redirect('/');
            } else {
                $this->Session->setFlash('Tiliäsi ei voitu luoda.', 'default', array(), 'auth');
            }
        }
    }

    function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allow('register', 'index', 'view', 'info');
        $this->Auth->deny('change');
    }

    /*
     * Change password when logged in
     */
    function change() {
        if(!empty($this->data)) {
            $user = $this->User->find('first', array('conditions' => array('User.id' => $this->Auth->user('id'))));
            if($user['User']['password'] == AuthComponent::password($this->data['User']['password_old'])) {
                $this->User->id = $this->Auth->user('id');
                if($this->data['User']['password'] == $this->data['User']['password_confirm']) {
                    $this->User->saveField('password', $this->data['User']['password']);
                    $this->Session->setFlash('Salasana vaihdettu', 'default', array(), 'auth');
                    $this->redirect('/');
                } else {
                    $this->Session->setFlash('Tarkista uusi salasana.', 'default', array(), 'auth');
                }
            } else {
                $this->Session->setFlash('Tarkista vanha salasana.', 'default', array(), 'auth');
            }
        }
    }

    /*
     * Currently only works to *samk.fi -domains, additional configuration probably needed.
     * Also needs "forgot" in users-table. varchar(11), default null
     */
    function forgot() {
        if(!empty($this->data)) {
            $user = $this->User->find('first', array('conditions' => array('User.username' => $this->data['User']['username'])));
            if($user['User']['id'] > 0) {
                $used = 1;
                while($used) {
                    $token = substr(str_shuffle('abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'), 0, 10);
                    if($this->User->find('first', array('conditions' => array('User.forgot' => $token))) == false) {
                        $used = 0;
                    }
                }
                $this->User->id = $user['User']['id'];
                $this->User->saveField('forgot', $token);
                $email = new CakeEmail('smtp');
                $email->template('forgot', 'laatupuntari')
                      ->emailFormat('html')
                      ->viewVars(array('token' => $token))
                      ->from(array('noreply@laatupuntari.fi' => 'Laatupuntari'))
                      ->sender('noreply@laatupuntari.fi', 'Laatupuntari')
                      ->to($user['User']['username'])
                      ->subject('Laatupuntari - salasanan vaihto')
                      ->send();
                $this->Session->setFlash('Sähköposti lähetetty', 'default', array(), 'auth');
            } else {
                $this->Session->setFlash('Kirjoittamaasi sähköpostiosoitetta ei löytynyt', 'default', array(), 'auth');
            }
        }
    }

    /*
     * Receive password change token and show form for new password
     */
    function token() {
        if(isset($this->request->pass[0])) {
            $token = $this->request->pass[0];
            $user = $this->User->find('first', array('conditions' => array('User.forgot' => $token)));
            if($user['User']['id'] > 0) {
                $this->User->id = $user['User']['id'];
                $this->set('data', array('User' => $user['User'], 'Token' => $token));
            } else {
                $this->redirect('/');
            }
        } else {
            $this->redirect('/');
        }
    }
    /*
     * Handle password change requests, part of forgot password function
     * If passwords (new password and confirm password) don't match, redirect back to users/token
     * Token is stored in a hidden field just for that
     */
    function tokenize() {
        if(!empty($this->data)) {
            $user = $this->User->find('first', array('conditions' => array('User.id' => $this->data['User']['id'])));
            $this->User->id = $this->data['User']['id'];
            if($this->data['User']['password'] == $this->data['User']['password_confirm']) {
                $this->User->saveField('password', $this->data['User']['password']);
                $this->User->saveField('forgot', null);
                $this->Session->setFlash('Salasana vaihdettu', 'default', array(), 'auth');
                $this->redirect('/');
            } else {
                $this->Session->setFlash('Tarkista uusi salasana.', 'default', array(), 'auth');
                $this->redirect('/users/token/'.$this->data['User']['token']);
            }
        }
    }

}
