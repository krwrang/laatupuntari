<?php
App::uses('Controller', 'Controller');
App::uses('CakeEmail', 'Network/Email');

/**
 * Created by Kristian Wrang <kristian.2.wrang@samk.fi>.
 * Date: 13.3.2012
 * 
 * This source code is licensed under the EUPL, Version 1.1 only (the “Licence”).
 * You may not use, modify or distribute this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * <http://joinup.ec.europa.eu/software/page/eupl/licence-eupl>
 * A copy is also distributed with this source code.
 * Unless required by applicable law or agreed to in writing, software distributed under the
 * Licence is distributed on an “AS IS” basis, without warranties or conditions of any kind.
 */
class AppController extends Controller {
    public $helpers = array('Js', 'Form', 'Html', 'Session', 'Recaptcha.Recaptcha');
    var $components = array('RequestHandler', 'Auth', 'Session', 'Recaptcha.Recaptcha');

    /*
     * For now we want nearly all functions usable by everyone regardless of whether they are logged in or not.
     * Deny specific functions/pages in their respectable controllers
     */
    function beforeFilter() {
        $this->Auth->allow('*');
    }

    function parseData($tmp, $type) {
        foreach($this->data as $input => $val) {
            if($input != "kypsyysaste" && $input != "category" && $input != "reportid") {
                $itmp = explode('_', $input);
                $idx = end($itmp);
                if($itmp[0] == 'textinput' && strlen($val) == 0) {
                    //Save a bit of time by not going deeper in the if-structure
                } else if($itmp[0] == 'score' && $type == 0) {
                	//We only want scores in pdf's so we need this because of the last "default else"
                } else if(isset($tmp[$idx]) && !is_array($tmp[$idx])) {
                    $tmp2 = $tmp[$idx];
                    if($itmp[0] == 'textinput' && strlen($val) > 0) {
                        if($tmp2 == $itmp[1]) {
                            $tmp[$idx] = $itmp[1].'|'.$itmp[2].'|'.$val;
                        } else {
                            $tmp[$idx] = array($tmp2, $itmp[1].'|'.$itmp[2].'|'.$val);
                        }
                    } else if($itmp[0] == 'input') {
                        $tmp[$idx] = array($tmp2, $val);
                    }
                } else if(isset($tmp[$idx]) && is_array($tmp[$idx])) {
                    if($itmp[0] == 'textinput' && strlen($val) > 0) {
                        if(($key = array_search($itmp[1], $tmp[$idx])) !== FALSE) {
                            $tmp[$idx][$key] = $itmp[1].'|'.$itmp[2].'|'.$val;
                        } else {
                            $tmp[$idx][] = $itmp[1].'|'.$itmp[2].'|'.$val;
                        }
                    } else if($itmp[0] == 'input') {
                        $tmp[$idx][] = $val;
                    }
                } else if(count($idx) < 3) {
                    if($itmp[0] == 'input') {
                        $tmp[$idx] = $val;
                    } else if($itmp[0] == 'textinput' && strlen($val) > 0) {
                        $tmp[$idx] = $itmp[1].'|'.$itmp[2].'|'.$val;
                    } else {
                        $tmp[$input] = $val;
                    }
                }
            } else {
                $tmp[$input] = $val;
            }
        }
        return $tmp;
    }
    /*
     * Every answer has a weight that is used in report analysis. Here we make things easier by calculating a maximum score for each question.
     * That way we don't need to store a maximum that would need to be changed every time we update scores in a question series.
     */
    function maxWeight($questions) {
        foreach($questions as $key => $value) {
            $max = 0;
            if($value['Question']['type'] == 'checkbox') {
                foreach($value['Answer'] as $i => $val) {
                    if($val['weight'] > 0) {
                        $max += $val['weight'];
                    }
                }
            } else {
                foreach($value['Answer'] as $i => $val) {
                    if($max < $val['weight']) {
                        $max = $val['weight'];
                    }
                }
            }
            $questions[$key]['Question']['maxweight'] = $max;
        }
        return $questions;
    }

}
