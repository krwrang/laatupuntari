<?php
/**
 * Created by Kristian Wrang <kristian.2.wrang@samk.fi>.
 * Date: 25.9.2012
 * Time: 09:36 
 * 
 * This source code is licensed under the EUPL, Version 1.1 only (the “Licence”).
 * You may not use, modify or distribute this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * <http://joinup.ec.europa.eu/software/page/eupl/licence-eupl>
 * A copy is also distributed with this source code.
 * Unless required by applicable law or agreed to in writing, software distributed under the
 * Licence is distributed on an “AS IS” basis, without warranties or conditions of any kind.
 */

class FeedbacksController extends AppController {

	
	/*
	 * Use Auth-component to prevent anonymous users from viewing certain pages.
	 */
	function beforeFilter() {
		parent::beforeFilter();
		$this->Auth->allow('list', 'view');
		$this->Auth->deny('save', 'add', 'remove');
	}
	
	function add() {
		$this->set('userid', $this->Auth->user('id'));
	}
	
	function save() {
		if(!empty($this->data)) {
			$userid = -1;
			if($this->Auth->user('id') > 0) {
				$userid = $this->Auth->user('id');
			}
			date_default_timezone_set('Europe/Helsinki');
			$private = 0;
			if(isset($this->data['Feedback']['private'])) {
				$private = $this->data['Feedback']['private'];
			}
			$this->Feedback->create();
			$this->Feedback->set(array(
					'text'      => $this->data['Feedback']['text'],
					'userid'    => $userid,
					'star'		=> $this->data['Feedback']['star'],
					'date'      => date("Y-m-d H:i:s"),
					'private'   => $private
			));
			$this->Feedback->save();
			$this->Session->setFlash('Kiitos palautteestasi!', 'default', array(), 'auth');
			$this->redirect('/feedbacks/listFeedback');
		}
	}
	
	function remove() {
		if(isset($this->request->pass[0])) {
			if(isset($this->request->pass[1]) && $this->request->pass[1] == 1) {
				$feedb = $this->Feedback->find('first', array(
															  'recursive' => '1',
														      'conditions' => array('Feedback.id' => $this->request->pass[0])));

				if($this->Auth->user('admin') == 1 || $feedb['User']['id'] == $this->Auth->user('id')) {
					$this->Feedback->delete($this->request->pass[0], false);
					$this->Session->setFlash('Palaute poistettu', 'default', array(), 'auth');
					$this->redirect('/feedbacks/listFeedback');
				}
				$this->Session->setFlash('Palautetta ei poistettu!', 'default', array(), 'auth');
				$this->redirect('/feedbacks/listFeedback');
			}
		} else {
			$this->Session->setFlash('Palautetta ei poistettu!', 'default', array(), 'auth');
			$this->redirect('/feedbacks/listFeedback');
		}
	}
	
	function listFeedback() {
		if($this->Auth->user('admin') == 1) {
			$conditions = array();
		} else if($this->Auth->user('admin') == 0 && $this->Auth->user('id') > 0) {
			$conditions = array('OR' => array(
					'Feedback.private' => '0',
					'Feedback.userid' => $this->Auth->user('id')));
		} else {
			$conditions = array('Feedback.private' => '0');
		}
		
		$count = $this->Feedback->find('count', array('conditions' => $conditions));
		$offset = 0;
		$perpage = 10;
		if(isset($this->request->pass[0])) {
			$perpage = $this->request->pass[0];
		}
		$pages = ceil($count/$perpage);
		$page = 0;
		if(isset($this->request->pass[1])) {
			$page = $this->request->pass[1];
		}
		$offset = $page * $perpage;
		$list = $this->Feedback->find('all', array(
												   'offset' => $offset,
												   'recursive' => '1',
												   'limit' => $perpage,
												   'conditions' => $conditions
									  ));
		$this->set('data', $list);
		$this->set('perpage', $perpage);
		$this->set('page', $page);
		$this->set('pages', $pages);
		$this->set('userAdmin', $this->Auth->user('admin'));
		$userid = -1;
		if($this->Auth->user('id') > 0) {
			$userid = $this->Auth->user('id');
		}
		$this->set('userid', $userid);
	}
}
