<?php
/**
 * Created by Kristian Wrang <kristian.2.wrang@samk.fi>.
 * Date: 31.1.2012
 * Time: 12:45
 * 
 * This source code is licensed under the EUPL, Version 1.1 only (the “Licence”).
 * You may not use, modify or distribute this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * <http://joinup.ec.europa.eu/software/page/eupl/licence-eupl>
 * A copy is also distributed with this source code.
 * Unless required by applicable law or agreed to in writing, software distributed under the
 * Licence is distributed on an “AS IS” basis, without warranties or conditions of any kind.
 */

class ReportsController extends AppController {

	
	/*
	 * Use Auth-component to prevent anonymous users from viewing certain pages.
	 */
	function beforeFilter() {
		parent::beforeFilter();
		$this->Auth->allow('save', 'debuglist');
		$this->Auth->deny('listReports', 'view');
	}
	
	/*
	 * List all reports for the user that's logged in.
	 */
	function listReports() {
		$reports = $this->Report->find('all', array('conditions' => array('userid' => $this->Auth->user('id'))) );
		$this->set('data', $reports);
	}
	
	/*
	 * Just make sure the report the user is trying to view is his/her own
	 * if not, redirect to home.
	 * All real report building happens elsewhere.
	 */
	function view() {
		if(isset($this->params['pass']['0'])) {
			$reportId = $this->params['pass']['0'];
			$userid = $this->Auth->user('id');
			$report = $this->Report->find('first', array('conditions' => array('id' => $reportId)));
			if($report['Report']['userid'] != $userid) {
				$this->redirect('/');
			}
			$report['Report']['data'] = unserialize($report['Report']['data']);
			$this->set('data', $report);
		} else {
			$this->redirect('/');
		}
	}
	
	/*
	 * Read report-data from database, then send it with the help of JSON
	 */
	function getData() {
		$this->autoRender = false;
		
		if($this->request->is('ajax')) {
			$report = $this->Report->find('first', array(
					'conditions' => array(
							'id' => $this->request->query['id']),
					'recursive'  => '1'
			));
			$report['Report']['data'] = unserialize($report['Report']['data']);
			echo json_encode($report);
		} else {
			$this->redirect($this->referer());
		}
	}

    /*
     * reports/debuglist shows a nice print of how the reports are stored into the database
     */
    function debuglist() {
        $reports = $this->Report->find('list', array('fields' => array('data')));
        foreach($reports as $input => $val) {
            $tmp[] = unserialize($val);
        }
        $this->data =  $tmp;
    }
    
    function save() {
        $this->autoRender = false;
        if($this->RequestHandler->isAjax() && !empty($this->data) && count($this->data) < 100) {
            $tmp = array();
            $tmp = $this->parseData($tmp, 0);
            $userid = -1;
            if($this->Auth->user('id') > 0) {
                $userid = $this->Auth->user('id');
            }
            
            date_default_timezone_set('Europe/Helsinki');
            $this->Report->create();
            $this->Report->set(array(
                            'sessionid' => $this->Session->id(null), //We'll get the sessionid here when the project is far enough to actually start sessions.
                            'data'      => serialize($tmp),
                            'userid'    => $userid,
            				'date'      => date("Y-m-d H:i:s")
            ));
            $this->Report->save();
        }
    }
}
