<?php
    echo $this->Form->create(array('action' => 'login'));
    echo $this->Form->input('username', array('label' => array(
                                                         'text' => 'Sähköpostiosoite',
                                                         'class' => 'UsernameLabel'
                                                         )
                                             ));
    echo $this->Form->input('password', array('type' => 'password',
    										  'label' => array(
    													 'text' => 'Salasana',
    													 'class' => 'PasswordLabel'
    													 )
    										  ));
    echo $this->Form->end('Kirjaudu sisään');
    echo $this->Html->link('Unohditko salasanasi?', array('controller' => 'users', 'action' => 'forgot'));
?>
