<?php
	echo $this->Session->flash('auth');
    echo $this->Form->create(array('action' => 'register'));
    echo $this->Form->input('username', array('label' => array(
    										                'text' => 'Sähköpostiosoite',
    										                'class' => 'EmailLabel'
    										                )
    									  ));
    echo $this->Form->input('password', array('label' => array(
    														'text' => 'Salasana',
    														'class' => 'PasswordLabel'
    														)
    										 ));
    echo $this->Form->input('password_confirm', array('label' => array(
    														'text' => 'Salasana uudelleen',
    														'class' => 'PasswordLabel'
    														),
    												  'type' => 'password'
    												 ));
    echo $this->Recaptcha->show();
    echo $this->Recaptcha->error();
    echo $this->Form->end('Rekisteröidy');
?>