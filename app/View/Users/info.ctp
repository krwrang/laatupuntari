<h1>Tervetuloa arvioimaan Living Labiasi!</h1>
<p>Laatupuntari on suunniteltu Living Lab -toiminnan laadun mittaamiseen. 
Laatupuntari on toiminnan suunnittelun ja arvioinnin väline ja ohjaa 
Living Lab -toimijoita kiinnittämään huomiota kehitettäviin asioihin. 
Arviointi pohjautuu Ammattikorkeakoulujen neloskierre –hankkeessa 
luotuun käsikirjaan ja sen pohjalta syntyneeseen kriteeristöön. 
Laatupuntarissa voit itse vaikuttaa arvioinnin laajuuteen sekä 
valita arvioitavan osa-alueen.
Laatupuntari ei sisällä kriteerien taustatietoa tai suoria linkkejä 
Living Lab ammattikorkeakouluissa -käsikirjaan, mutta löydät käsikirjan 
kappaleista samanlaisen jaon ja ryhmittelyn kuin Laatupuntarissa.
Käsikirjan löydät osoitteesta fnoll.wordpress.com</p>
<h4>Arviointi</h4>
Muista kirjautua tai rekisteröityä, jos et ole pelkästään kokeilemassa. 
Voit tutustua työkaluun käyttämällä sitä ilman rekisteröitymistä, mutta 
näin vastatessa tietoa ei talleteta, eikä sitä pääse myöhemmin tarkastelemaan. 
Arviointiprosessi alkaa arvioitavan osa-alueen valitsemisella. Sen jälkeen 
muutamalla taustakysymyksellä valitaan arvioitava vaihe tai taso. 
Tämän jälkeen käynnistyy varsinainen kysely, joka koostuu valinta- ja 
monivalintakysymyksistä sekä tarkentavista kentistä. Kyselyn etenemistä 
voit seurata kysymysten päällä olevasta järjestysnumerosta. Voit siirtyä 
eteenpäin tai palata edelliseen kysymykseen painikkeiden avulla. 
Lopuksi voit vielä tallentaa koneellesi PDF-muotoisen raportin tuloksistasi.
<h4>Omat arvioinnit</h4>
Voit seurata Living Labisi kehittymistä valitsemalla oman tunnuksesi 
ylävalikosta sekä ”Listaa aikaisemmat kyselyt”. Kyselyt listautuvat 
aikajärjestyksessä. Kyselyt eivät tallennu kirjautumattomilla käyttäjillä!
<h4>Itsearviointina</h4>
Living Lab -operaattorin roolissa oleva täyttää tai teettää ulkopuolisen 
arvioinnin, mutta ulkopuolisen tahon on usein vaikea vastata osioiden 
kysymyksiin ilman vahvaa tietämystä kyseisen Living Labin toiminnasta.
<h4>Yksin tai ryhmässä</h4>
Arvioinnin voi suorittaa myös ryhmänä, palaverissa tai workshopissa, 
jolloin hyödynnetään monipuolisempaa asiantuntemusta sekä yhteistyön 
tuomia näkökulmia. Tällöin arviointi toimii myös oppimisprosessina, 
jossa tiimi saa palautteen muilta, löytää kehityskohteita ja alkaa jo 
kehittää yhdessä ratkaisuja ongelmakohtien löytämisen lisäksi.
<h4>Arvioinnin kesto</h4>
Tyypillinen yhden osa-alueen kyselyyn vastaaminen kestää puolesta 
tunnista puoleentoista tuntiin.
<h4>Rekisteröityminen</h4>
Tallentaaksesi arvioinnin tai tutkiaksesi myöhemmin omia arviointejasi 
sinun täytyy olla kirjautuneena järjestelmään. Kirjautuminen vaatii 
rekisteröitymisen: kirjautumistunnuksena toimii sähköpostiosoite sekä 
henkilökohtainen salasana. Salasanan vaihtaminen tapahtuu 
klikkaamalla omaa tunnustasi yläriviltä.
<h4>Arvioinnin pisteyttäminen ja arvosanojen muodostaminen</h4>
Eri kriteereille on määritelty perusarvot sekä raja-arvot joiden 
perusteella arviointi pisteytetään seuraaviin luokkiin: Hyvä, Kehitä 
ja Panosta. Vastaustesi perusteella löydät Living Labisi vahvuudet 
ja pystyt kehittämään niitä osa-alueita, joita muissa Living Labeissä 
on pidetty tärkeänä kyseisellä tasolla.
<h4>Laatupuntarin suunnittelu ja toteutus</h4>

<p>Laatupuntari on syntynyt Ammattikorkeakoulujen neloskierre –hankkeen tuloksena.
Materiaalin ja sisällön tekoon ovat osallistuneet hankkeen projektikoordinaattorit ja muut toimijat työpajoissa yhdessä sekä erikseen käyttämällä verkkopohjaisia yhteiskirjoittamisen jaettuja alustoja.
</p><p>
Sisällöntuotanto: Ammattikorkeakoulujen neloskierre –hanke <a href="http://www.neloskierre.fi/">(http://www.neloskierre.fi)</a><br/>
Suunnittelu- ja toteutustyöryhmä: Heikki Riikonen (pj.), Olli Ojala, Ari Haapanen, Sanna Haapanen, Jouni Silfver, Kari Laine, Heikki Kaplas & Tiina Ferm<br/>
Koodaus: Kristian Wrang, Razvan Constantinescu & Iiro Uusitalo<br/>
Graafinen suunnittelu: Mari Heikkinen & Marjo Jussila</p>
<p>
Copyright 2012, Ammattikorkeakoulujen neloskierre –hanke (2009-2013) <a href="http://www.neloskierre.fi/">http://www.neloskierre.fi/</a> <br/>
Työkalun ohjelmiston lähdekoodi on julkaistu EUPL 1.1 lisenssillä EUPL 1.1, <a href="http://joinup.ec.europa.eu/system/files/FI/EUPL%20v.1.1%20-%20Lisenssi.pdf">http://joinup.ec.europa.eu/system/files/FI/EUPL%20v.1.1%20-%20Lisenssi.pdf</a>.<br/>
Laatupuntari on toteutettu Manner-Suomen ESR-ohjelma on rahoittamassa hankkeessa, jossa on luotu parhaiden käytäntöjen käsikirja sekä työvälineitä ja toimintaohjeita kysyntä- ja käyttäjälähtöiseen innovaatiotoimintaan eli Living Lab –toimintaan, oppimalla olemassa olevista ammattikorkeakoulujen Living Labeista.
</p><p>
Ammattikorkeakoulujen neloskierre –hanke:<br/>
Projektipäällikkö<br/>
Javaro Oy: Janne Orava</p>
<p>
HAAGA-HELIA ammattikorkeakoulu (koordinaattori) / Pasila Living Lab: Sakariina Heikkanen, Henna Kemppainen, Mari Österberg
</p><p>
Arcada – Nylands svenska yrkeshögskola / Espoo Living Lab Innovations (ELLI): Mervi Hernberg, Jyrki Kettunen, Marianne Tast
</p><p>
Hämeen ammattikorkeakoulu / Virvelinranta: Merja Salminen, Leena Koskimäki
</p><p>
Jyväskylän ammattikorkeakoulu / Lutakko Living Lab: Petri Liukkonen, Juha Ruuska, Saara Linna, Kaisa Sulasalmi
</p><p>
Kemi-Tornion ammattikorkeakoulu/ Rajalla Living Lab: Marjo Jussila, Tomi Sipola, Mari Heikkinen
</p><p>
Kymenlaakson ammattikorkeakoulu / Kasarminmäki Living Lab: Jorma Fagerström, Ari Haapanen, Sanna Haapanen, Jouni Silfver
</p><p>
Laurea-ammattikorkeakoulu / User Driven Innovation Center (UnIC): Olli Vilkki
</p><p>
Oulun seudun ammattikorkeakoulu / OULLabs: Heikki Riikonen
</p><p>
Saimaan ammattikorkeakoulu / Saimia Living Lab: Asko Kilpeläinen, Kirsi Viskari
</p><p>
Satakunnan ammattikorkeakoulu / Innovaatiolaboratorio: Kari Laine, Heikki Kaplas, Iiro Uusitalo
</p><p>
Seinäjoen ammattikorkeakoulu / Agro Living Lab, Habitcenter Living Lab: Kari Salo, Antti Silvola, Vuokko Takala-Schreib, Janne Aho
</p><p>
Tampereen ammattikorkeakoulu / PractiCo(r) Living Lab: Sanna Lehtokannas, Kristiina Lilja, Sinikka Seppänen, Ella Kallio
</p><p>
Turun ammattikorkeakoulu / Turku University of Applied Sciences Living Lab for Well-being and ICT (TWICT): Tiina Ferm, Olli Ojala, Razvan Constantinescu</p>
<p>
Lähdekoodi saatavilla osoitteesta<br/>
<a href="https://bitbucket.org/krwrang/laatupuntari">https://bitbucket.org/krwrang/laatupuntari</a>
</p>