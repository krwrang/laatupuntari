<h2><?php echo $this->Html->link('Listaa aikaisemmat kyselyt', '/reports/listReports'); ?></h2>

<h2>Salasanan vaihto</h2>
<?php
	echo $this->Session->flash('auth');
    echo $this->Form->create(array('action' => 'change'));
    echo $this->Form->input('password_old', array('label' => Array(
    												               'text' => 'Vanha salasana',
    												               'class' => 'PasswordLabel'
    												               ),
    										      'type' => 'password'));
    echo $this->Form->input('password', array('label' => Array(
     														   'text' => 'Uusi Salasana',
     														   'class' => 'PasswordLabel'
     														   ),
     										  'type' => 'password'));
    echo $this->Form->input('password_confirm', array('label' => Array(
    															       'text' => 'Uusi Salasana uudelleen',
    															       'class' => 'PasswordLabel'
    															       ),
    												  'type' => 'password'));
    echo $this->Form->end('Vaihda salasana');
?>
