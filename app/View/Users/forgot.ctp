<h2>Unohtuneen salasanan vaihto</h2>
<?php
    echo $this->Session->flash('auth');
    echo $this->Form->create(array('action' => 'forgot'));
    echo $this->Form->input('username', array('label' => 'Sähköpostiosoite'));
    echo $this->Form->end('Olen unohtanut salasanani');
?>