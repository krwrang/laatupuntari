<h2>Salasanan vaihto</h2>
<?php
	echo $this->Session->flash('auth');
    echo $this->Form->create(array('action' => 'tokenize'));
    echo $this->Form->hidden('id', array('value' => $data['User']['id']));
    echo $this->Form->hidden('token', array('value' => $data['Token']));
    echo $this->Form->input('password', array('label' => Array(
    														   'text' => 'Uusi Salasana',
    														   'class' => 'PasswordLabel'
    														   ),
    										  'type' => 'password'));
    echo $this->Form->input('password_confirm', array('label' => Array(
    															       'text' => 'Uusi Salasana uudelleen',
    																   'class' => 'PasswordLabel'
    																   ),
    												  'type' => 'password'));
    echo $this->Form->end('Vaihda salasana');
?>
