<h2>Anna palautetta Laatupuntarille</h2>
<?php
    echo $this->Form->create(array('action' => 'save'));
    echo $this->Form->input('text', array('label' => array(
    												       'text' => 'Palaute',
    												       'class' => 'FeedbackLabel'
    												        )
    					                  ));
	echo $this->Form->radio('star', array('0', '1', '2', '3', '4', '5'), 
									array('legend' => 'Arviosi (0-5 tähteä)',
								    	 'class' => 'StarLabel',
										  'value' => '0'));
	echo $this->Form->input('private', array('type' => 'checkbox',
											 'hiddenField' => false,
												'class' => 'PrivateLabel',
												'label' => array('text' =>'Yksityinen palaute')
												));
	echo '<br/>';
    echo $this->Form->end('Lähetä');
?>
<h3>Ohje:</h3>
<p>
Kirjoita palautelaatikkoon palautteesi, vapaa sana.
<br/>
Jos haluat, että palautteesi näkyy vain pääkäyttäjille, valitse 'yksityinen'. Muussa tapauksessa palaute on julkinen.
<br/>
Muista vielä arvioida Laatupuntaria asteikolla 0 - 5 tähteä, viiden ollessa paras arvio.
</p>