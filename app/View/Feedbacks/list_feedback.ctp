<h2> Käyttäjien antamaa palautetta <?php if($userid > -1) { echo ' - '.$this->Html->link('Haluatko antaa palautetta?', array('controller' => 'feedbacks', 'action' => 'add')); } ?></h2>
<?php
	$stars = array('zero', 'one', 'two', 'three', 'four', 'five');
	foreach($data as $row => $value) {
		echo '<div class="feedback">';
		echo '<table><tr><td valign="top">';
		echo '<div class="'.$stars[$value['Feedback']['star']].'star"></div>';
		$email = $value['User']['username'];
		if($userAdmin != 1 && $userid != $value['User']['id']) {
			$emailArray = split('@', $email);
			$emailArray[0] = preg_replace('/[a-ö]/', '*', $emailArray[0]);
			$email = $emailArray[0].'@'.$emailArray[1];
		}
		echo '<div class="fbemail">'.$email.'</div>';
		echo '<div class="fbdate">'.$value['Feedback']['date'].'</div>';
		if($userAdmin == 1 || $userid == $value['User']['id']) {
			echo '<div class="fbremove">'.$this->Html->link("Poista", array("controller" => "feedbacks", "action" => "remove", $value["Feedback"]["id"])).'</div>';
		}
		echo '</td><td>';
		echo '<div class="fbtext"><textarea readonly>'.$value['Feedback']['text'].'</textarea></div>';
		echo '</td></tr></table>';
		echo '</div>';
	}
	echo 'Sivut: ';
	for($i = 0; $i < $pages; $i++) {
		$pageNow = $i + 1;
		if($i != $page) {
			echo $this->Html->link($pageNow, array('controller' => 'feedbacks', 'action' => 'listFeedback', $perpage, $i));
		} else {
			echo $pageNow;
		}
		echo '  ';
	}
?>
