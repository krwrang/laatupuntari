<h2>Haluatko varmasti poistaa valitun palautteen?</h2>
<?php
echo $this->Html->link("Poista", array("controller" => "feedbacks", "action" => "remove", $this->params['pass'][0], 1),
							     array("class" => "button small hand"));
echo "  ";
echo $this->Html->link("Peruuta", array("controller" => "feedbacks", "action" => "listFeedback"),
								  array("class" => "button small hand"));
?>