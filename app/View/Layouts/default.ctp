<?php echo $this->Html->docType('xhtml11'); ?>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
        <title><?php echo $title_for_layout?></title>
        <?php echo $this->Html->css('style'); ?>
        <?php echo $this->Html->scriptBlock("var root = '".$this->Html->url('/')."';"); ?>
        <?php echo $scripts_for_layout ?>
</head>
<body>
    <div class="container">
        <div class="header">
        <?php echo $this->Html->script(array('jquery.min','laatu')); ?>
		<?php //echo $this->Session->flash(); ?>
		<?php echo $this->Session->flash('auth'); ?>
            <ul class="top-menu">
                <li><?php echo $this->Html->link('Etusivu', '/'); ?></li>
                <li><?php echo $this->Html->link('Palaute', array('controller' => 'feedbacks', 'action' => 'listFeedback')); ?></li>
                <?php
                	$user = AuthComponent::user('id');
                	if(!isset($user)) {
						echo '<li>'.$this->Html->link('Kirjaudu', array('controller' => 'users', 'action' => 'login')).'</li>';
						echo '<li>'.$this->Html->link('Rekisteröidy', array('controller' => 'users', 'action' => 'register')).'</li>';
                   	} else {
						echo '<li id="UserNameLi">'.$this->Html->link('Käyttäjätili', array(
						       							              'controller' => 'users',
						       							              'action' => 'change'),
						       							              array('class' => 'UserNameLink')).'</li>';
						echo '<li>'.$this->Html->link('Kirjaudu ulos', array('controller' => 'users', 'action' => 'logout')).'</li>';
                	}
                ?>
                <li><?php echo $this->Html->link('Tietoja', array('controller' => 'users', 'action' => 'info')); ?></li>
            </ul>
            <?php
            	if(!isset($user)) {
            		echo '<div title="Kirjautumattomana tekemäsi kyselyt eivät tallennu!">Et ole kirjautunut!</div>';
            	}
            ?>
        </div>
        <div class="banner">
            <?php echo $this->Html->link($this->Html->image('logo.png', array('alt' => 'Laatupuntari')), array('controller' => 'pages', 'action' => 'display', 'home'), array('escape' => false));  ?>
        </div>
        <div class="content">
            <?php echo $content_for_layout ?>
        </div>
        <div class="footer">
            <?php echo $this->Html->image('logot-footer.png', array('alt' => 'Laatupuntari')); ?>
        </div>
    </div>
</body>
</html>
