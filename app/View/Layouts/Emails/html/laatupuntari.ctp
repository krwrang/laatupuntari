<?php echo $this->Html->docType('xhtml11'); ?>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <title><?php echo $title_for_layout?></title>
</head>
<body>
    <div class="container">
        <div class="content">
            <?php echo $content_for_layout ?>
        </div>
    </div>
</body>
</html>
