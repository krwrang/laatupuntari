<?php
header('Content-type: application/pdf');
App::import('Vendor', 'dompdf', array('file' => 'dompdf'.DS.'dompdf_config.inc.php'));
$dompdf = new DOMPDF();
$dompdf->load_html($content_for_layout, 'UTF8');
$dompdf->render();
$dompdf->stream("Laatupuntari_".time()."_raportti.pdf");
