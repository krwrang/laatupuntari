<p><strong>Tervetuloa Laatupuntariin!</strong></p>
<p>Laatupuntari kuvaa, mikä Living Lab- toiminnassa on laadukasta ja minkä osa-alueiden
kehittämiseen tulisi jatkossa kiinnittää erityistä huomiota. Laatupuntaria voidaan
käyttää tarkistuslistana Living Lab -toiminnassa hyviksi havaittuihin käytänteisiin.</p>

<p class="category">
    <strong>Valitse arvioitava osa-alue</strong>
    <div class="orange">
        <a class="toiminto" href="<?php echo $this->Html->url(array('controller' => 'questions', 'action' => 'toiminta')); ?>">
            <strong>Living Lab- toiminta</strong>
            <p>Miten Living Lab- toiminta on suunniteltu ja määritetty ja mitä käytäntöjä noudatetaan?</p>
        </a>
        <a class="case" href="<?php echo $this->Html->url(array('controller' => 'questions', 'action' => 'kase')); ?>">
            <strong>Living Lab- case</strong>
            <p>Miten Living Lab- casen toteutus on suunniteltu ja toteutettu?</p>
        </a>
        <a class="opetus" href="<?php echo $this->Html->url(array('controller' => 'questions', 'action' => 'opetus')); ?>">
            <strong>Living Lab ja opetus</strong>
            <p>Miten Living Lab- toiminta integroituu ammattikorkeakoulun opetukseen ja TKI- toimintaan?</p>
        </a>
    </div>
</p>
