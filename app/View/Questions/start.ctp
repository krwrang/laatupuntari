<?php echo $this->Html->script(array('jquery.min', 'laatupuntari'), false); ?>

<form action="<?php echo $this->Html->url(array('controller' => 'questions', 'action' => 'createpdf')); ?>" id="questionsForm" method="post" target="_blank">
    <div id="questions">
	    <div id="report" class="hidden">
	    	<div class="percentagegood"><strong><span id="pgood"></span><font size="6">%</font><font size="4"><br />Hyvä</font></strong></div>
			<div class="percentageok"><strong><span id="pok"></span><font size="6">%</font><font size="4"><br />Kehitä</font></strong></div>
			<div class="percentagebad"><strong><span id="pbad"></span><font size="6">%</font><font size="4"><br />Panosta</font></strong></div>
			<div class="percentageother"><strong><span id="pother"></span><font size="6">%</font><font size="4"><br />Muut</font></strong></div>
			<div id="showgood" class="green"></div>
			<div id="showok" class="yellow"></div>
			<div id="showbad" class="red"></div>
			<div id="showother" class="grey"></div>
			<div id="bottomgreen" class="traffic-green"></div>
			<div id="bottomyellow" class="traffic-yellow"></div>
			<div id="bottomred" class="traffic-red"></div>
			<div id="bottomgrey" class="traffic-grey"></div>
			<div class="orange2"></div>
		</div>
    	<input type="text" hidden class="hidden" id="category" name="category" value="<?php echo $this->params['pass'][0]; ?>">
    	<div class="question" id="toimiala">
            <div class="subject">Mikä on Living Lab -organisaation toimiala?</div>
            <div class="answer">
                <p><label><input type="checkbox" name="toimiala[]" value="0" /> terveys ja hyvinvointi</label></p>
                <p><label><input type="checkbox" name="toimiala[]" value="1" /> rakentaminen ja asuminen</label></p>
                <p><label><input type="checkbox" name="toimiala[]" value="2" /> ICT ja media</label></p>
                <p><label><input type="checkbox" name="toimiala[]" value="3" /> matkailu ja majoitus</label></p>
                <p><label><input type="checkbox" name="toimiala[]" value="4" /> julkiset palvelut</label></p>
                <p><label><input type="checkbox" name="toimiala[]" value="5" /> kulttuuri</label></p>
                <p><label><input type="checkbox" name="toimiala[]" value="6" /> monialainen</label></p>
                <p><label><input type="checkbox" name="toimiala[]" class="control" value="7" /> muu, mikä? </label><label><input type="text" name="textinput_7_toimiala"></label></p>
                <p><label><input type="checkbox" name="toimiala[]" class="excl" value="8" /> ei ole määritelty</label></p>
            </div>
            <div class="next button center hand small">Seuraava →</div>
        </div>
        <div class="question hidden" id="ika">
            <div class="subject">Kuinka kauan Living Lab -organisaatio on toiminut?</div>
            <div class="answer">
                <p><label><input type="radio" name="ika" value="0" checked="checked" /> alle 1 vuoden</label></p>
                <p><label><input type="radio" name="ika" value="1" /> 1 - 2 vuotta</label></p>
                <p><label><input type="radio" name="ika" value="2" /> 3 - 5 vuotta</label></p>
                <p><label><input type="radio" name="ika" value="3" /> yli 5 vuotta</label></p>
            </div>
            <div class="next button center hand small">Seuraava →</div>
        </div>
        <div class="question hidden" id="kypsyysaste">
        	<?php
        		// Väliaikainen ratkaisu, kunnes kysymyssarjoista on yleistiedot kannassa
	        	if($this->params['pass'][0] == 'toiminta') {
	        	?>
	            <div class="subject">Arvioi, mikä on Living Lab -toiminnan ja ekosysteemin kypsyystaso. Valintasi perusteella sivusto tarjoaa kypsyystasoon soveltuvat kysymykset.</div>
	            <div class="answer">
	                <p><label><input type="radio" name="kypsyysaste" value="0" checked="checked" /> Living Lab -toimintaa suunnitellaan (valmisteilla)</label></p>
	                <p><label><input type="radio" name="kypsyysaste" value="1" /> Toteutusta on suunniteltu ja yksittäisiä caseja toteutettu (käynnistymässä)</label></p>
	                <p><label><input type="radio" name="kypsyysaste" value="2" /> Living Lab -toiminta ja ekosysteemi on vakiintunut (käynnissä)</label></p>
	            </div>
	        <?php
	        	} else if($this->params['pass'][0] == 'case') {
	        	?>
	        	<div class="subject">Mikä on Living Lab -casen vaihe? Tässä kategoriassa voit valita useamman vaiheen kerralla. Valintasi perusteella sivusto tarjoaa vaiheeseesi soveltuvat kysymykset.</div>
	            <div class="answer">
	                <p><label><input type="checkbox" name="kypsyysaste[]" value="0" /> Tilausvaihe</label></p>
	                <p><label><input type="checkbox" name="kypsyysaste[]" value="1" /> Käynnistysvaihe</label></p>
	                <p><label><input type="checkbox" name="kypsyysaste[]" value="2" /> Toteutusvaihe</label></p>
	                <p><label><input type="checkbox" name="kypsyysaste[]" value="3" /> Arviointivaihe</label></p>
	            </div>
	        <?php
	            } else if($this->params['pass'][0] == 'opetus') {
	            ?>
	            <div class="subject">Opetusvaihtoehto? Valintasi perusteella sivusto tarjoaa vaiheeseesi soveltuvat kysymykset.</div>
	            <div class="answer">
	                <p><label><input type="radio" name="kypsyysaste" value="0" checked="checked" /> Opetuksen integroinnin suunnittelu</label></p>
	                <p><label><input type="radio" name="kypsyysaste" value="1" /> Toteutus</label></p>
	                <p><label><input type="radio" name="kypsyysaste" value="2" /> Arviointi ja palaute</label></p>
	                <p><label><input type="radio" name="kypsyysaste" value="3" /> Living Lab -toiminnan ja opetuksen kehittäminen</label></p>
	            </div>
	        <?php
	        	}
	        	?>
            <div class="next button center hand small">Seuraava →</div>
        </div>
    </div>
</form>
