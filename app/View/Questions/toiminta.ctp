<p>Tässä laatukriteeristön osiossa arvioidaan, kuinka hyvin Living Lab 
-toimintaa on suunniteltu ja määritetty vastaajan ammattikorkeakoulussa, 
millaisia käytäntöjä toiminnassa noudatetaan sekä millä kypsyystasolla 
ammattikorkeakoulun Living Labissa-toiminta on.</p>

<p>Tähän laatukriteerien osioon vastaa Living Lab -operaattori.</p>
<p>Kyselyssä kysytään ensin taustakysymyksiä Living Labista. Kysely on jaettu kolmeen eri kypsyysvaiheeseen:</p>
<ul>
    <li>Valmisteilla oleva Living Lab</li>
    <li>Käynnistymässä oleva Living Lab</li>
    <li>Käynnissä oleva Living Lab</li>
</ul>

<p>Vastaaja voi itse valita, haluaako täyttää kaikkien eri kypsyysvaiheiden laatukriteerien 
kysymykset tai vaihtoehtoisesti keskittyä pelkästään jo tiedossa olevan kypsyysasteen 
mukaisiin kysymyksiin. Työkalua voi käyttää tarkistuslistana
ja sen avulla voi saada palautetta siitä,  millä osa-alueilla toiminta on hyvin määriteltyä ja missä on vielä parannettavaa.</p>

<strong>Täyttöohje</strong>
<pMikäli kysymys ei liity mielestäsi arvioitavaan Living Lab -toimintaan, 
valinta “ei relevantti kysymys” jättää kysymyksen analyysin ulkopuolelle. 
“Ei osaa sanoa” -valinnalla on tietyissä kysymyksissä merkitystä analyysin tuloksiin.</p>


<?php echo $this->Html->link('Aloita testi', array('controller' => 'questions', 'action' => 'start', 'toiminta'), array('class' => 'button')); ?>
