<p>Tämä laatupuntarin osio on tarkoitettu erityisesti ammattikorkeakoulun Living Lab 
-operaattorille, joka haluaa selvittää, kuinka hyvin oman Living Labin toiminta 
integroituu ammattikorkeakoulun opetukseen, mitä käytäntöjä noudatetaan sekä miten 
käytännöt on toteutettu opetuksen ja OPS-suunnittelun eri vaiheissa. 
Laatupuntarin voi täyttää esimerkiksi kerran vuodessa, jolloin vastaaja saa tiedon, miten Living Lab 
-toiminnan integrointi on kehittynyt ammattikorkeakoulussa. Halutessaan laatupuntarin voi täyttää myös useammin.</p>


<p>Kysely on jaettu neljään eri osa-alueeseen:</p>
<ul>
    <li>Opetuksen integroinnin suunnittelu</li>
    <li>Toteutus</li>
    <li>Arviointi ja palaute</li>
    <li>Living Lab -toiminnan ja opetuksen kehittäminen</li>
</ul>

<p>Osio sisältää myös elementtejä muista laatukriteeristön osista: 
Living Lab -toiminta ja Living Lab -case, mutta keskittyy niiden arviointiin opetuksen kannalta.</p>

<strong>Täyttöohje</strong>
<p>Mikäli kysymys ei liity mielestäsi arvioitavaan Living Lab 
-toimintaan, valinta “ei relevantti kysymys” jättää kysymyksen analyysin ulkopuolelle. 
“Ei osaa sanoa” -valinnalla on tietyissä kysymyksissä merkitystä analyysin tuloksiin.</p>

<?php echo $this->Html->link('Aloita testi', array('controller' => 'questions', 'action' => 'start', 'opetus'), array('class' => 'button')); ?>
