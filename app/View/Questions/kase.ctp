<p>Tässä laatukriteeristön osiossa arvioidaan, miten case-prosessia toteutetaan 
vastaajan Living Labissa, millaisia käytäntöjä toiminnassa noudatetaan sekä 
millä kypsyystasolla case-prosessi ammattikorkeakoulun Living Labissa on.</p>

<p>Tähän laatukriteerien osioon vastaa Living Lab -operaattori.</p>
<p>Kysely on jaettu neljään eri case-prosessin vaiheeseen:</p>
<ul>
    <li>Tilausvaihe</li>
    <li>Käynnistysvaihe</li>
    <li>Toteutusvaihe</li>
    <li>Testausvaihe</li>
</ul>

<p>Vastaaja voi itse valita, haluaako täyttää kaikkien eri vaiheiden laatukriteerien kysymykset, 
vai täyttääkö hän kyselyn kokonaisuudessaan vasta case-prosessin arvioinnin valmistuttua. 
Työkalua voi käyttää tarkistuslistana ja sen avulla voi saada palautetta 
siitä, millä osa-alueilla on toiminta hyvin määriteltyä ja missä on vielä parannettavaa.</p>

<strong>Täyttöohje</strong>
<p>Mikäli kysymys ei liity mielestäsi arvioitavaan Living Lab -toimintaan, 
valinta “ei relevantti kysymys” jättää kysymyksen analyysin ulkopuolelle. 
“Ei osaa sanoa” -valinnalla on tietyissä kysymyksissä merkitystä analyysin tuloksiin.</p>

<?php echo $this->Html->link('Aloita testi', array('controller' => 'questions', 'action' => 'start', 'case'), array('class' => 'button')); ?>
