<?php
	$scoreColor = array("grey_small.png","red_small.png","yellow_small.png","green_small.png");
	$user = AuthComponent::user('id');
        if(!isset($user)) {
        	$answerer = "Ei määritelty";
        } else {
        	$answerer = AuthComponent::user('username');
        }
    $nQuestions = 0;
    $nGood = 0;
    $nOk = 0;
    $nBad = 0;
    $nOther = 0;
    foreach($data as $key => $value){
    	if($value['score'] == 0){
    		$nOther++;
    	} else if($value['score'] == 1){
    		$nBad++;
    	} else if($value['score'] == 2){
    		$nOk++;
    	} else if($value['score'] == 3){
    		$nGood++;
    	}
    	$nQuestions++;
    }
    $pGood = round(($nGood/$nQuestions)*100);
    $pOk = round(($nOk/$nQuestions)*100);
    $pBad = round(($nBad/$nQuestions)*100);
    $pOther = round(($nOther/$nQuestions)*100);
?>
<html>
    <head>
      <title>Raportti - Laatupuntari.fi</title>
        <style>
            body, html {
                padding:  0px;
                font-family: Times;
                margin: 20px 20px 20px 20px;
            }
            .logo {
                display: block;
                height: 100px;
                width: 100%;
                background: #8C8C8C;
            }
            .logo img {
                position: absolute;
                top: 20px;
                left: 20px;
            }
            .content {
                padding:  10px;
            }

            .footer {
                display: block;
                text-align: center;
            }
            .question img {
            	padding-top: 10px;
            }
            
            .percentagegood {
			    display: block;
			    text-align: center;
			    font-size: 60px;
			    color: #000;
			    line-height:0.65;
			    margin-top:85px;
			    z-index:3;
			}

			.percentageok {
			    display: block;
			    text-align: center;
			    font-size: 60px;
			    color: #000;
			    line-height:0.65;
			    margin-top:85px;
			    z-index:3;
			}
			
			.percentagebad {
			    display: block;
			    font-size: 60px;
			    text-align: center;
			    color: #000;
			    line-height:0.65;
			    margin-top:85px;
			    z-index:3;
			}
			
			.percentageother {
			    display: block;
			    font-size: 60px;
			    text-align: center;
			    color: #000;
			    line-height:0.65;
			    margin-top:85px;
			    z-index:3;
			}			            
			            
			.orange {
			    display:block;
			    background: #ff9e3d;
			    height: 116px;
			    border: none;
			    width: 1154px;
			    margin-top: 30px;
			    margin-left: -50px;
			    margin-bottom: 150px;
			    z-index:1;
			}
			.green {
			    display: block;
			    position: absolute;
			    background:  url('http://v1.laatupuntari.fi/img/green.png') no-repeat;
			    height: 200px;
			    width: 200px;
			    margin-left: 15px;
			    margin-top: -40px;
			    line-height: 2em;
			    z-index:2;
			}			
			.yellow {
			    display: block;
			    position: absolute;
			    background:  url('http://v1.laatupuntari.fi/img/yellow.png') no-repeat;
			    height: 200px;
			    width: 200px;
			    margin-left: 210px;
			    margin-top: -40px;
			    line-height: 2em;
			    z-index:2;
			}
			
			.red {
			    display: block;
			    position: absolute;
			    background:  url('http://v1.laatupuntari.fi/img/red.png') no-repeat;
			    height: 200px;
			    width: 200px;
			    margin-left: 400px;
			    margin-top: -40px;
			    line-height: 2em;
			    z-index:2; 
			}
			
			.grey {
			    display: block;
			    position: absolute;
			    background:  url('http://v1.laatupuntari.fi/img/white.png') no-repeat;
			    height: 200px;
			    width: 200px;
			    margin-left: 595px;
			    margin-top: -40px;
			    line-height: 2em;
			    z-index:2; 
			}

        </style>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    </head>
    <body>
    <div class="logo"><img src="http://v1.laatupuntari.fi/img/logo.png" alt="" /></div>
    <div class="content">
        Vastaaja: <?php echo $answerer; ?><br />
        <!--  Organisaatio: Ei määritelty<br /> -->
        Kategoria: <?php echo $category; ?><br />
        Living Labin kypsyys: <?php echo $kypsyys; ?>

        <p>Olet osallistunut Laatupuntari.fi -sivuston v1-version toiminnalliseen testaukseen ja arvioinut organisaatiosi Living Lab -toiminnan tasoa.</p>

        <p>Vastauksiesi yhteenveto on seuraava:</p>
        <div class="orange">
		<div id="showgood" class="green"><div class="percentagegood"><strong><span id="pgood"><?php echo $pGood; ?></span><font size="6">%</font><font size="4"><br />Hyvä</font></strong></div></div>
		<div id="showok" class="yellow"><div class="percentageok"><strong><span id="pok"><?php echo $pOk; ?></span><font size="6">%</font><font size="4"><br />Kehitä</font></strong></div></div>
		<div id="showbad" class="red"><div class="percentagebad"><strong><span id="pbad"><?php echo $pBad; ?></span><font size="6">%</font><font size="4"><br />Panosta</font></strong></div></div>
		<div id="showother" class="grey"><div class="percentageother"><strong><span id="pother"><?php echo $pOther; ?></span><font size="6">%</font><font size="4"><br />Muut</font></strong></div></div>
		</div>

        <?php
        foreach($data as $row => $value) {
            	?>
                   <h3 class="question"><?php 
                   echo '<img src="http://v1.laatupuntari.fi/img/'.$scoreColor[$value['score']].'" /> ';
                   echo $value['question']; ?></h3>
                   <p><strong>Vastasit:</strong> <?php echo $value['answer']; ?></p>
            	<?php
        }
        ?>
    </div>
    <div class="footer"><img src="http://v1.laatupuntari.fi/img/logot-footer.png" alt="" /></div>
    </body>
</html>
