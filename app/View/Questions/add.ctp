<?php
	echo $this->Html->script(array('jquery.min', 'laatupuntariadd'), false);

	echo $this->Form->create('Question');
	echo $this->Form->input('question');
	echo $this->Form->input('type', array(
		'options' => array('radio', 'checkbox', 'inputbox'),
		'empty' => '(Valitse)'
	));
	echo $this->Form->input('category', array(
		'options' => array('toiminta', 'case', 'opetus'),
		'empty' => '(Valitse)'
	));
	echo $this->Form->input('maturity', array(
		'options' => array('0', '1', '2', '3'),
		'empty' => '(Valitse)'
	));
	echo $this->Form->input('sort');
	echo '<div class="answ">';
	echo $this->Form->input('Answer.0.value', array(
		'label' => 'Vastausvaihtoehto'
	));
	echo $this->Form->input('Answer.0.weight', array(
		'label' => 'Painoarvo'
	));
	echo '<label for="Answer0excl">Poissulkeva vastaus</label>';
	echo $this->Form->checkbox('Answer.0.excl', array(
		'label' => 'Eksklusiivinen',
		'value' => 1,
		'hiddenField' => false,
		'class' => 'excl'
	));
	echo '</div><br/><span class="lisays"></span><br/><div class="add button small">Uusi vastausvaihtoehto</div><br/><br/>';
	echo $this->Form->end(array(
		'label' => 'Lisää tietokantaan',
		'class' => 'button small'
	));
?>
