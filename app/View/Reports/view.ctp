<?php echo $this->Html->script(array('jquery.min', 'laatupuntariview'), false); ?>

<form action="<?php echo $this->Html->url(array('controller' => 'questions', 'action' => 'createpdf')); ?>" id="questionsForm" method="post" target="_blank">
    <div id="questions">
	    <div id="report" class="hidden">
	    	<div class="percentagegood"><strong><span id="pgood"></span><font size="6">%</font><font size="4"><br />Hyvä</font></strong></div>
			<div class="percentageok"><strong><span id="pok"></span><font size="6">%</font><font size="4"><br />Kehitä</font></strong></div>
			<div class="percentagebad"><strong><span id="pbad"></span><font size="6">%</font><font size="4"><br />Panosta</font></strong></div>
			<div class="percentageother"><strong><span id="pother"></span><font size="6">%</font><font size="4"><br />Muut</font></strong></div>
			<div id="showgood" class="green"></div>
			<div id="showok" class="yellow"></div>
			<div id="showbad" class="red"></div>
			<div id="showother" class="grey"></div>
			<div id="bottomgreen" class="traffic-green"></div>
			<div id="bottomyellow" class="traffic-yellow"></div>
			<div id="bottomred" class="traffic-red"></div>
			<div id="bottomgrey" class="traffic-grey"></div>
			<div class="orange2"></div>
		</div>
    	<input type="text" hidden class="hidden" id="reportid" name="reportid" value="<?php echo $this->params['pass'][0]; ?>">
    	<input type="text" hidden class="hidden" id="category" name="category" value="<?php echo $data['Report']['data']['category']; ?>">
    	<div class="question hidden" id="kypsyysaste">
    		<div class="subject">Vaihe</div>
	        <div class="answer">
	        <?php
	        	if(is_array($data['Report']['data']['kypsyysaste'])) {
	           		foreach($data['Report']['data']['kypsyysaste'] as $idx => $val) {
	           			echo '<p><label><input type="checkbox" checked name="kypsyysaste[]" value="'.$val.'" /> '.$val.'</label></p>';
	           	    }
	           	} else {
	           		echo '<p><label><input type="checkbox" checked name="kypsyysaste" value="'.$data['Report']['data']['kypsyysaste'].'" /> '.$data['Report']['data']['kypsyysaste'].'</label></p>';
	   	    	}
	   	    ?>
	   	    </div>
       	</div>
    </div>
</form>