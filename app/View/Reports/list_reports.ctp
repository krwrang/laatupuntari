<h2> Aikaisemmin täyttämäsi kyselyt </h2>
<table width="500px">
<tr>
<td><strong>Päivä ja aika</strong></td>
<td><strong>Kategoria</strong></td>
<td><strong>Kypsyystaso</strong></td>
</tr>
<?php 
	$maturities = array('toiminta' => array('Valmisteilla', 'Käynnistymässä', 'Käynnissä'),
				        'case' => array('Tilausvaihe', 'Käynnistysvaihe', 'Toteutusvaihe', 'Arviointivaihe'),
				 		'opetus' => array('Opetuksen integroinnin suunnittelu', 'Toteutus', 'Arviointi ja palaute', 'Living Lab -toiminnan ja opetuksen kehittäminen'));
	foreach($data as $row => $value) {
		$questionnaire = unserialize($value['Report']['data']);
		$category = $questionnaire['category'];
		if(is_array($questionnaire['kypsyysaste'])) {
            $kypsyysaste = '';
            foreach($questionnaire['kypsyysaste'] as $index => $val){
            	$kypsyysaste = $kypsyysaste.' '.$maturities[$questionnaire['category']][$val].',';
            }
            $kypsyysaste = substr($kypsyysaste, 0, strlen($kypsyysaste) - 1);
            $maturity = $kypsyysaste;
        } else {
           	$maturity = $maturities[$category][$questionnaire['kypsyysaste']];
        }
		echo "<tr><td>".$this->Html->link($value['Report']['date'], array('controller' => 'reports', 'action' => 'view', $value['Report']['id']))."</td>";
		echo "<td>".ucfirst($category)."</td>";
		echo "<td>".$maturity."</td></tr>";
	}
?>
</table>