/**
 * This source code is licensed under the EUPL, Version 1.1 only (the “Licence”).
 * You may not use, modify or distribute this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * <http://joinup.ec.europa.eu/software/page/eupl/licence-eupl>
 * A copy is also distributed with this source code.
 * Unless required by applicable law or agreed to in writing, software distributed under the
 * Licence is distributed on an “AS IS” basis, without warranties or conditions of any kind.
 */

$(window).ready(function() {
    var first = null;
    var scores = new Array();

    var nGood = 0;
    var nOk = 0;
    var nBad = 0;
    var nOther = 0;
    var nQuestions = 0;
    
    function addHoverEffect(div) {
    	$(div).stop().fadeTo(0,0);
    	$(div).hover(
			function() {
				$(this).stop().animate({"opacity": "1"}, "medium");
			},
			function() {
				$(this).stop().animate({"opacity": "0"}, "medium");
			}
		);  
    }
    
    $("#showbad").live('click', function() {
    	//$(this).parent().prepend("There are " + nBad + "/" + nQuestions + " aspect(s) that need improvement:");
    	//Hides all questions and show only the ones tagged as "bad"
    	$(".question").stop(true, true).fadeOut();
    	$(".bad").stop(true, true).fadeIn();
    	
    	//Reactivate buttons hover effect
    	addHoverEffect("#showok");
    	addHoverEffect("#showgood");
    	addHoverEffect("#showother");
    	//Deactivates hover effect and makes red dot visible (pressed)
    	$("#showbad").stop().fadeTo(0,1);
    	$("#showbad").unbind('mouseenter mouseleave');
    });
    
    //Shows only ok questions
    $("#showok").live('click', function() {
    	$(".question").stop(true, true).fadeOut();
    	$(".ok").stop(true, true).fadeIn();
    	addHoverEffect("#showbad");
    	addHoverEffect("#showgood");
    	addHoverEffect("#showother");
    	$("#showok").stop().fadeTo(0,1);
    	$("#showok").unbind('mouseenter mouseleave');
    });
    
    //Shows only good questions
    $("#showgood").live('click', function() {
    	$(".question").stop(true, true).fadeOut();
    	$(".good").stop(true, true).fadeIn();
    	addHoverEffect("#showok");
    	addHoverEffect("#showbad");
    	addHoverEffect("#showother");
    	$("#showgood").stop().fadeTo(0,1);
    	$("#showgood").unbind('mouseenter mouseleave');
    });
    
    $("#showother").live('click', function() {
    	$(".question").stop(true, true).fadeOut();
    	$(".other").stop(true, true).fadeIn();
    	addHoverEffect("#showok");
    	addHoverEffect("#showbad");
    	addHoverEffect("#showgood");
    	$("#showother").stop().fadeTo(0,1);
    	$("#showother").unbind('mouseenter mouseleave');
    });

    function scoreQuestions() {
    	$(".question").each(function(key, val) {
    		if($(this).attr('id') != 'kypsyysaste') {
    			var weight = 0;
    			var qid = $(this).attr('id');
    			$(this).find('input[name*=input]').each(function(iKey, iVal) {
    				if($(this).attr('checked')) {
    					weight += scores[qid][$(this).attr('value')];
    				}
    			});
    			/*
    			 * If you don't have weight-values in the table, this will produce NaN
    			 */
    			var percentage = 0;
    			percentage = weight / scores[qid]['max'];
    			nQuestions++;
    			/* 
    			 * This is just an example, you'll need to change actual percentages here
    			 * classnames likewise are just examples
    			 */
    			var score; //Used for sending scoring to pdf creation
    			if(percentage >= 0 && percentage < 0.4) {
    				$(this).addClass("bad");
    				nBad++;
    				score = 1;
    			} else if(percentage >= 0.4 && percentage < 0.7) {
    				$(this).addClass("ok");
    				nOk++;
    				score = 2;
    			} else if(percentage >= 0.7) {
    				$(this).addClass("good");
    				nGood++;
    				score = 3;
    			} else {
    				$(this).addClass("other");
    				nOther++;
    				score = 0;
    			}
    			
    			$(this).find(".score").first().attr('value', score);
    		}
        });
    }

    function showReport() {
    	$("#questions").prepend('<h1 class="endTitle">Lopputulos:</h1>');

        $("#toimiala").remove();
        $("#ika").remove();

        scoreQuestions();
        var percentageGood = Math.round((nGood/nQuestions)*100);
        var percentageOk = Math.round((nOk/nQuestions)*100);
        var percentageBad = Math.round((nBad/nQuestions)*100);
        var percentageOther = Math.round((nOther/nQuestions)*100);

        $("#pgood").text(percentageGood);
        $("#pok").text(percentageOk);
        $("#pbad").text(percentageBad);
        $("#pother").text(percentageOther);

    	addHoverEffect("#showbad");
    	addHoverEffect("#showok");
    	addHoverEffect("#showgood");
    	addHoverEffect("#showother");
    	
    	/*
    	 * This would be good, so that the user doesn't go changing values
    	 * However, checkboxes/radiobuttons that are disabled or readonly do not send
    	 * any data through form submit
    	 */
    	
        //$("input").attr("readonly", true);

        $(".question").each(function(key, val) {
            $(this).find('.prog').remove();
            $(this).find('.next').remove();
            $(this).find('.prev').remove();
            $(this).append('<hr />');
            $(this).hide();
        });

        $("#report").fadeIn();

        $("#questions").append('<a href="'+root+'/questions/valmis" class="button small">Valmis</a>');
        $("#questions").append('<div id="print" class="button small">Tulosta</div>');
        $("#questions").append('<input type="submit" value="Luo PDF-raportti" class="button small" />');
        $("#questions").append('<div class="qInfo"><h3>Ohje:</h3>Voit napsauttaa jotain neljästä pallosta nähdäksesi kysymykset, joiden pisteytys osui ko. kategoriaan.<br/>'+
        					   'Voit halutessasi tallentaa vastauksesi PDF-muodossa. Jos täytit kyselyn sisäänkirjautuneena, saat kyselyn näkyville käyttäjätilin kautta<br/>'+
        					   'Kun et enää halua tarkastella vastauksiasi, voit poistua sivulta. Vastaukset on jo tallennettu tietokantaan.');
    }

    /*
     * Enable/disable textboxes depending on their parent radiobuttons/checkboxes
     */
    $(".control").live('click', function() {
    	if ($(this).attr('type') == 'radio') {
    		$('input[name='+$(this).attr('name')+']').each(function(index) {
    			var name = $(this).attr('name').split('_');
    	    	var textname = 'textinput_'+$(this).attr('value');
    	    	if ($('input[name*="'+textname+'"][name*="'+name[1]+'"]').length > 0){
    	    		if($(this).attr('checked')) {
        				$('input[name*="'+textname+'"][name*="'+name[1]+'"]').attr('disabled', false);
                	} else {
                		$('input[name*="'+textname+'"][name*="'+name[1]+'"]').attr('disabled', true);
                		$('input[name*="'+textname+'"][name*="'+name[1]+'"]').attr('value', '');
                	}
    	    	}
    		});
    	} else {
        	var inputname = $(this).attr('name');
        	inputname = inputname.split('[');
    		var name = inputname[0].split('_');
        	var textname = 'textinput_'+$(this).attr('value');
        	if ($('input[name*="'+textname+'"][name*="'+name[1]+'"]').length > 0){
    			if($(this).attr('checked')) {
    				$('input[name*="'+textname+'"][name*="'+name[1]+'"]').attr('disabled', false);
            	} else {
            		$('input[name*="'+textname+'"][name*="'+name[1]+'"]').attr('disabled', true);
            		$('input[name*="'+textname+'"][name*="'+name[1]+'"]').attr('value', '');
            	}
    		}
    	}
    });

    /*
     * If you have an answer, that cannot be chosen with other answers
     * like "not relevant to me" you set the 'excl' value in mysql for that question
     * to '1' and this function makes sure you cannot choose an exclusive answer with other answers.
     */
    $(".excl").live('click', function() {
    	var allow = 1;
    	var thisvalue = $(this).attr('value');
    	var checked = $(this).attr('checked');
    	$(this).parent().parent().parent().find('input').each(function(index) {
			if($(this).attr('checked') && $(this).attr('value') != thisvalue) {
				allow = 0;
			}
		});
    	if(allow == 0){
    		$(this).attr('checked', false);
    		alert('Et voi valita tätä vastausta muiden vastausten kanssa!');
    	} else {
    		$(this).parent().parent().parent().find('input[name*=input]').each(function(index) {
    			if($(this).attr('value') != thisvalue) {
    				if(checked) {
    					$(this).attr('disabled', true);
    				} else {
    					$(this).attr('disabled', false);
    				}
    			}
    		});
    	}
    });

    $("div#print").live('click', function() {
        window.print();
    });

    $("div.prev").live('click', function() {
    	$(this).parent().fadeOut().stop(true, true).fadeOut(function() {
    		$(parent).prevAll(".question:first").stop(true, true).fadeIn();
    	});
    });

    $(".def").live('click', function() {
    	/* TODO: Once we have definitive questions of type checkbox, rewrite this function to accommodate
    	 * ie. make a function that just changes the state of matching questions according to the checkbox just checked/unchecked
    	 */
    	var id = $(this).parent().parent().parent().attr('id');
    	$('[class*=" '+id+'"]').removeClass('question').addClass('delete');
    	//First hide all possible sub-questions
    	var sub = "|" + $(this).attr('value') + "|";
    	$('[class*="'+id+'"][class*="'+sub+'"]').removeClass("delete").addClass("question");
    	//Then show the set of sub-questions that was chosen.
    });

    $("div.next").live('click', function() {
    	var answered = 0;
    	$(this).parent().find('input').each(function(iKey, iVal) {
			if($(this).attr('checked')) {
				answered = 1;
			}
		});
    	if(answered == 0) {
    		$(this).parent().find('input[type=text]').each(function(iKey, iVal) {
    			if($(this).attr('value').length > 0) {
    				answered = 1;
    			}
    		});
    	}
    	if(answered == 1) {
	        var parent = $(this).parent().fadeOut();
	        if ($(this).parent().attr('id') == "kypsyysaste") {
	        	var maturities = [];
	        	$(this).prev().find(":checked").each(function(iKey, iVal) {
	        		maturities.push($(this).val());
	        	});
	            $.getJSON(root+"/questions/getData",
	                {
	                    maturity: maturities,
	                    category: $("#category").val()
	                }, function(data) {
	
	                    $.each(data, function(key, val) {
	                        //Store the total score of each question in the array
	                        scores[val.Question.id] = new Array();
	                        scores[val.Question.id]["max"] = parseInt(val.Question.maxweight);
	                        if (key == "0")
	                            first = val.Question.id;
	                        var definitive = '';
	                        if(val.Question.def == 1){
	                        	definitive = 'def';
	                        }
	                        var question = '';
	                        if(val.Question.subid.length > 0){
	                        	question = '<div class="delete hidden '+ val.Question.subid +'" id="' + val.Question.id + '">' +
	                        	           '<div class="prog">['+ (key + 1) +'/'+data.length+']</div>' +
	                        	           '<div class="subject">'+ val.Question.question +'</div>';
	                    	} else {
	                    		question = '<div class="question hidden" id="' + val.Question.id + '">' +
	         	           	   			   '<div class="prog">['+ (key + 1) +'/'+data.length+']</div>' +
	         	           	   			   '<div class="subject">'+ val.Question.question +'</div>';
	                    	}
	
	                        question += '<input class="hidden score" type="text" name="score_'+val.Question.id+'" />';
	                        $.each(val.Answer, function(answerKey, answerVal) {
	                            //Store the individual weights of answers to the array
	                            scores[val.Question.id][answerKey] = parseInt(answerVal.weight);
	                            var arrayAddon = '';
	                            var disabled = 'disabled';
	                            var excl = '';
	                            if(answerVal.excl == 1 && val.Question.type == "checkbox") {
	                            	excl = 'excl';
	                            }
	                            if(val.Question.type == "checkbox") {
	                            	arrayAddon = '[]';
	                            }
	                            if(val.Question.type == "inputbox"){
	                            	disabled = '';
	                            }
	                            var value = answerVal.value;
	                            var re = new RegExp("\{[a-ö]*\}(?!\_)");
	                            var m = re.exec(value, 'i');
	
	                            while(m != null) {
	                            	value = value.replace(re, '</label><label><input type="text" '+disabled+' name="textinput_'+answerKey+'_'+m+'_'+val.Question.id+'" />');
	                            	m = re.exec(value, "i");
	                            }
	                            if(val.Question.type == "inputbox") {
	                            	question += '<p><label>'+ value + '</label></p>';
	                            } else {
	                            	question += '<p><label><input class="control ' + definitive + ' ' + excl + '" value="'+ answerKey +'" type="'+ val.Question.type +'"' +
	                            				'name="input_'+val.Question.id+arrayAddon+'" /> '+ value +'</label></p>';
	                            }
	                        });
	                        if (key == "0"){
	                        	question += '<div class="next button center hand small">Seuraava →</div>';
	                        } else {
	                        	question += '<div class="prev button center hand small">← Edellinen</div>' +
	                        				'<div class="next button center hand small">Seuraava →</div>';
	                        }
	
	                        $("#questions").append(question);
	                    });
	
	                    $(parent).fadeOut(function() {
	                       $("#"+first).fadeIn();
	                    });
	                }
	            );
	        } else {
	        	$(parent).fadeOut(function() {
	        		$(parent).nextAll(".question:first").stop(true, true).fadeIn();
		            if($(parent).nextAll(".question").length == 0) {
			           	$(".delete").remove();
			            $.post(root+"/reports/save", $("#questionsForm").serialize(), function(data) {
			                 	//alert("Data Loaded: " + data);
			            })  //Alert-statements ready for debugging if necessary.
			               	.success(function()  { /*alert("second success");*/ })
			                .error(function(jqXHR, textStatus, errorThrown) { alert(textStatus + " - " + errorThrown); })
			                .complete(function() { /*alert("complete");*/ });

			            showReport();
		            }
		        });
	        }
    	} else {
    		alert("Et ole vastannut kysymykseen");
    	}
    });
});
