/**
 * This source code is licensed under the EUPL, Version 1.1 only (the “Licence”).
 * You may not use, modify or distribute this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * <http://joinup.ec.europa.eu/software/page/eupl/licence-eupl>
 * A copy is also distributed with this source code.
 * Unless required by applicable law or agreed to in writing, software distributed under the
 * Licence is distributed on an “AS IS” basis, without warranties or conditions of any kind.
 */

$(window).ready(function() {
    var scores = new Array();

    var nGood = 0;
    var nOk = 0;
    var nBad = 0;
    var nOther = 0;
    var nQuestions = 0;
    
    load();
    
    function addHoverEffect(div) {
    	$(div).stop().fadeTo(0,0);
    	$(div).hover(
			function() {
				$(this).stop().animate({"opacity": "1"}, "medium");
			},
			function() {
				$(this).stop().animate({"opacity": "0"}, "medium");
			}
		);  
    }
    
    $("#showbad").live('click', function() {
    	//$(this).parent().prepend("There are " + nBad + "/" + nQuestions + " aspect(s) that need improvement:");
    	//Hides all questions and show only the ones tagged as "bad"
    	$(".question").stop(true, true).fadeOut();
    	$(".bad").stop(true, true).fadeIn();
    	
    	//Reactivate buttons hover effect
    	addHoverEffect("#showok");
    	addHoverEffect("#showgood");
    	addHoverEffect("#showother");
    	//Deactivates hover effect and makes red dot visible (pressed)
    	$("#showbad").stop().fadeTo(0,1);
    	$("#showbad").unbind('mouseenter mouseleave');
    });
    
    //Shows only ok questions
    $("#showok").live('click', function() {
    	$(".question").stop(true, true).fadeOut();
    	$(".ok").stop(true, true).fadeIn();
    	addHoverEffect("#showbad");
    	addHoverEffect("#showgood");
    	addHoverEffect("#showother");
    	$("#showok").stop().fadeTo(0,1);
    	$("#showok").unbind('mouseenter mouseleave');
    });
    
    //Shows only good questions
    $("#showgood").live('click', function() {
    	$(".question").stop(true, true).fadeOut();
    	$(".good").stop(true, true).fadeIn();
    	addHoverEffect("#showok");
    	addHoverEffect("#showbad");
    	addHoverEffect("#showother");
    	$("#showgood").stop().fadeTo(0,1);
    	$("#showgood").unbind('mouseenter mouseleave');
    });
    
    $("#showother").live('click', function() {
    	$(".question").stop(true, true).fadeOut();
    	$(".other").stop(true, true).fadeIn();
    	addHoverEffect("#showok");
    	addHoverEffect("#showbad");
    	addHoverEffect("#showgood");
    	$("#showother").stop().fadeTo(0,1);
    	$("#showother").unbind('mouseenter mouseleave');
    });
    
    function scoreQuestions() {
    	$(".question").each(function(key, val) {
    		if($(this).attr('id') != 'kypsyysaste') {
    			var weight = 0;
    			var qid = $(this).attr('id');
    			$(this).find('input[name*=input]').each(function(iKey, iVal) {
    				if($(this).attr('checked')) {
    					weight += scores[qid][$(this).attr('value')];
    				}
    			});
    			/*
    			 * If you don't have weight-values in the table, this will produce NaN
    			 */
    			var percentage = 0;
    			percentage = weight / scores[qid]['max'];
    			nQuestions++;
    			/* 
    			 * This is just an example, you'll need to change actual percentages here
    			 * classnames likewise are just examples
    			 */
    			var score;
    			if(percentage >= 0 && percentage < 0.4) {
    				$(this).addClass("bad");
    				nBad++;
    				score = 1;
    			} else if(percentage >= 0.4 && percentage < 0.7) {
    				$(this).addClass("ok");
    				nOk++;
    				score = 2;
    			} else if(percentage >= 0.7) {
    				$(this).addClass("good");
    				nGood++;
    				score = 3;
    			} else {
    				$(this).addClass("other");
    				nOther++;
    				score = 0;
    			}
    			
    			$(this).find(".score").first().attr('value', score);
    		}
        });
    }
    
    function showReport() {
    	$("#questions").prepend('<h1 class="endTitle">Lopputulos:</h1>');
    	
        scoreQuestions();
        var percentageGood = Math.round((nGood/nQuestions)*100);
        var percentageOk = Math.round((nOk/nQuestions)*100);
        var percentageBad = Math.round((nBad/nQuestions)*100);
        var percentageOther = Math.round((nOther/nQuestions)*100);

        $("#pgood").text(percentageGood);
        $("#pok").text(percentageOk);
        $("#pbad").text(percentageBad);
        $("#pother").text(percentageOther);

    	addHoverEffect("#showbad");
    	addHoverEffect("#showok");
    	addHoverEffect("#showgood");
    	addHoverEffect("#showother");
    	
    	/*
    	 * This would be good, so that the user doesn't go changing values
    	 * However, checkboxes/radiobuttons that are disabled or readonly do not send
    	 * any data through form submit
    	 */
    	
        //$("input").attr("readonly", true);

        $(".question").each(function(key, val) {
            $(this).find('.prog').remove();
            $(this).find('.next').remove();
            $(this).find('.prev').remove();
            $(this).append('<hr />');
            $(this).hide();
        });

        $("#report").fadeIn();

        $("#questions").append('<a href="'+root+'" class="button small">Valmis</a>');
        $("#questions").append('<div id="print" class="button small">Tulosta</div>');
        $("#questions").append('<input type="submit" value="Luo PDF-raportti" class="button small" />');
    }

    $("div#print").live('click', function() {
        window.print();
    });

    function load() {    	
	    $.getJSON(root+"/reports/getData",
	    	{
	    		id: $("#reportid").val()
	    	}, function(report) {    
	            $.getJSON(root+"/questions/getData",
	                {
	                    maturity: report.Report.data.kypsyysaste,
	                    category: report.Report.data.category
	                }, function(data) {
	
	                    $.each(data, function(key, val) {
	                        //Store the total score of each question in the array
	                        scores[val.Question.id] = new Array();
	                        scores[val.Question.id]["max"] = parseInt(val.Question.maxweight);
	                        var definitive = '';
	                        if(val.Question.def == 1){
	                        	definitive = 'def';
	                        }
	                        var question = '';
	                        var reportdata = report.Report.data[val.Question.id];
	                        if(val.Question.subid.length > 0 && typeof(reportdata) != "undefined"){
	                        	question = '<div class="question hidden '+ val.Question.subid +'" id="' + val.Question.id + '">' +
                 	           			   '<div class="subject">'+ val.Question.question +'</div>';
	                        } else if(val.Question.subid.length > 0){
	                        	question = '<div class="delete hidden '+ val.Question.subid +'" id="' + val.Question.id + '">' +
	                        	           '<div class="subject">'+ val.Question.question +'</div>';
	                    	} else {
	                    		question = '<div class="question hidden" id="' + val.Question.id + '">' +
	         	           	   			   '<div class="subject">'+ val.Question.question +'</div>';
	                    	}
	
	                        
	                        question += '<input class="hidden score" type="text" name="score_'+val.Question.id+'" />';
	                        $.each(val.Answer, function(answerKey, answerVal) {
	                            //Store the individual weights of answers to the array
	                            scores[val.Question.id][answerKey] = parseInt(answerVal.weight);
	                            var arrayAddon = '';
	                            var disabled = 'disabled';
	                            var answer = '';
	                            
	                            if(reportdata instanceof Array) {
	                            	for(var idx in reportdata){
	                            		var answertemp = reportdata[idx].split("|");
	                            		if(answertemp[0] == answerKey) {
		                            		answer = answertemp;
		                            		break;
		                            	}
	                            	}
	                            } else if(typeof(reportdata) != "undefined") {
	                            	var answertemp = reportdata.split("|");
	                            	if(answertemp[0] == answerKey) {
	                            		answer = answertemp;
	                            	}
	                            }
	                            
	                            if(val.Question.type == "checkbox") {
	                            	arrayAddon = '[]';
	                            }
	                            if(val.Question.type == "inputbox"){
	                            	disabled = '';
	                            }
	                            var value = answerVal.value;
	                            var re = new RegExp("\{[a-ö]*\}(?!\_)");
	                            var m = re.exec(value, 'i');
	
	                            while(m != null) {
	                            	var textvalue = '';
	                            	if(answer[1] == m) {
	                            		textvalue = answer[2];
	                            	}
	                            	value = value.replace(re, '</label><label><input type="text" '+disabled+' text="'+textvalue+'" name="textinput_'+answerKey+'_'+m+'_'+val.Question.id+'" />');
	                            	m = re.exec(value, "i");
	                            }
	                            
	                            var checkedvalue = '';
	                            if(answer != '') {
	                            	checkedvalue = 'checked';
	                            }
	                            if(val.Question.type == "inputbox") {
	                            	question += '<p><label>'+ value + '</label></p>';
	                            } else {
	                            	question += '<p><label><input class="control ' + definitive + '" value="'+ answerKey +'" type="'+ val.Question.type +'"' +
	                            				'name="input_'+val.Question.id+arrayAddon+'" '+checkedvalue+' /> '+ value +'</label></p>';
	                            }
	                        });
	                        $("#questions").append(question);
	                    });
	                    $(".delete").remove();
	                    showReport();
	                }
	            );
	    });    
    }
});
