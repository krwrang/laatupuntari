/**
 * This source code is licensed under the EUPL, Version 1.1 only (the “Licence”).
 * You may not use, modify or distribute this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * <http://joinup.ec.europa.eu/software/page/eupl/licence-eupl>
 * A copy is also distributed with this source code.
 * Unless required by applicable law or agreed to in writing, software distributed under the
 * Licence is distributed on an “AS IS” basis, without warranties or conditions of any kind.
 */

$(window).ready(function() {
	var amount = 0;

    $("div.add").live('click', function() {
        $('.answ').clone().first().insertBefore('.lisays');
        amount++;
        /* Clone one answer box and the associated weight value -box and modify their attributes so that CakePHP
         * recognizes data correctly and the HasMany -relationship  between
         * questions and answers remains intact. Makes storing to database much easier.
         */
        $('.answ:last').children('.input').children('label').attr('for', 'Answer'+amount+'Value');
        $('.answ:last').children('.input').children('textarea').attr('name', 'data[Answer]['+amount+'][value]');
        $('.answ:last').children('.input').children('textarea').attr('id', 'Answer'+amount+'Value');
        $('.answ:last').children('.input').children('input').attr('name', 'data[Answer]['+amount+'][weight]');
        $('.answ:last').children('.input').children('input').attr('id', 'Answer'+amount+'Weight');
        $('.answ:last').children('.excl').attr('name', 'data[Answer]['+amount+'][excl]');
        $('.answ:last').children('.excl').attr('id', 'Answer'+amount+'excl');
    });
});
