<?php
/* Answer Test cases generated on: 2011-11-29 02:02:31 : 1322524951*/
App::uses('Answer', 'Model');

/**
 * Answer Test Case
 *
 */
class AnswerTestCase extends CakeTestCase {
/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array('app.answer', 'app.question');

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();

		$this->Answer = ClassRegistry::init('Answer');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Answer);

		parent::tearDown();
	}

}
