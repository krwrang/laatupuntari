<?php
/* Contollers Test cases generated on: 2011-11-23 10:55:48 : 1322038548*/
App::uses('Contollers', 'Controller');

/**
 * TestContollers *
 */
class TestContollers extends Contollers {
/**
 * Auto render
 *
 * @var boolean
 */
	public $autoRender = false;

/**
 * Redirect action
 *
 * @param mixed $url
 * @param mixed $status
 * @param boolean $exit
 * @return void
 */
	public function redirect($url, $status = null, $exit = true) {
		$this->redirectUrl = $url;
	}
}

/**
 * Contollers Test Case
 *
 */
class ContollersTestCase extends CakeTestCase {
/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();

		$this->Contollers = new TestContollers();
		$this->->constructClasses();
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Contollers);

		parent::tearDown();
	}

}
